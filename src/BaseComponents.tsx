import React from 'react';
import './App.css';

interface ButtonProps {
  id: string;
  value: string;
  action: (event: React.SyntheticEvent) => void;
  color?: 'green' | 'red'
}
export class Button extends React.Component<ButtonProps> {
  render() {
    return (
      <button
        onClick={(event) => this.props.action(event)}
        className={this.props.color !== undefined ? this.props.color : 'neutral'}
        key={this.props.id}>
        {this.props.value}
      </button>
    );
  }
}

interface ModalProps {
  display: boolean;
  id: string;
  header: string | null;
  content: React.ReactNode;
  buttons: React.ReactNode;
}
export class Modal extends React.Component<ModalProps> {
  render() {
    return (
      <div id={this.props.id + '-modal-bg'} className={this.props.display ? 'modal-bg modal-display' : 'modal-bg modal-hide'}>
        <div id={'modal'} className={'modal'}>
          <div className='modal-head'><h1>{this.props.header}</h1></div>
          <div className='modal-content'>
            {this.props.content}
          </div>
          <div className='modal-buttonbar'>{this.props.buttons}</div>
        </div>
      </div>
    );
  }
}
