import './App.css';

import Game from './Game';
import { YatzyMaxi } from './GameTypes';

function App() {
  return (
    <div className='app'>
      <Game gameType={new YatzyMaxi()}/>
    </div>
  );
}

export default App;
