import { cloneDeep } from 'lodash';

import { Score, GameType } from './GameTypes'

class Player {
    name: string;
    id: string;
    scores: Map<string, Score>;
    constructor(name: string, id: string, gameType: GameType) {
        this.name = name;
        this.id = id;
        this.scores = cloneDeep(gameType.scores);
    }
}

export default Player;
