import Player from './Player';

export interface Score {
  name: string;
  value?: number;
  valueOk?: number | null;
  valueFuction?: (player: Player) => (number | undefined);
  rules: Array<(value: number) => boolean>;
  validDice: (dice: number[] | never) => number[];
  diceFunction?: (die: number, index: number) => number[];
  numberOfDice?: number;
}

export interface GameType {
  scores: Map<string, Score>;
  name: string;
}

export class YatzyStandard implements GameType {
  scores: Map<string, Score>;
  name: string = 'YatzyStandard';

  constructor() {
    this.scores = new Map<string, Score>();
    this.scores.set('ones', {
      name: 'Ettor',
      valueOk: 1 * 3,
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value <= 5;
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value % 1 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(5 - dice.length).fill(1);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('twos', {
      name: 'Tvåor',
      valueOk: 2 * 3,
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 2 * 5;
        },
        (value: number) => {
          return value % 2 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(5 - dice.length).fill(2);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('threes', {
      name: 'Treor',
      valueOk: 3 * 3,
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 3 * 5;
        },
        (value: number) => {
          return value % 3 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(5 - dice.length).fill(3);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('fours', {
      name: 'Fyror',
      valueOk: 4 * 3,
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 4 * 5;
        },
        (value: number) => {
          return value % 4 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(5 - dice.length).fill(4);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('fives', {
      name: 'Femmor',
      valueOk: 5 * 3,
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 5 * 5;
        },
        (value: number) => {
          return value % 5 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(5 - dice.length).fill(5);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('sixes', {
      name: 'Sexor',
      valueOk: 6 * 3,
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 6 * 5;
        },
        (value: number) => {
          return value % 6 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(5 - dice.length).fill(6);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('numbersum', {
      name: 'Summa',
      value: 0,
      valueFuction: function (player: Player) {
        const sum: number = (player.scores.get('ones')?.value || 0) +
             (player.scores.get('twos')?.value || 0) +
             (player.scores.get('threes')?.value || 0) +
             (player.scores.get('fours')?.value || 0) +
             (player.scores.get('fives')?.value || 0) +
             (player.scores.get('sixes')?.value || 0);
        return sum;
      },
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
      ],
      validDice: () => {
        return [];
      },
    });
    this.scores.set('bonus', {
      name: 'Bonus',
      valueFuction: function (player: Player) {
        let sum = 0;
        sum = (player.scores.get('ones')?.value || 0) +
             (player.scores.get('twos')?.value || 0) +
             (player.scores.get('threes')?.value || 0) +
             (player.scores.get('fours')?.value || 0) +
             (player.scores.get('fives')?.value || 0) +
             (player.scores.get('sixes')?.value || 0);
        if (sum >= 63) {
          return 50;
        } else {
          return undefined;
        }
      },
      rules: [
        (value: number) => {
          return Number.isInteger(value) || value === null;
        },
      ],
      validDice: () => {
        return [];
      },
    });
    this.scores.set('pair', {
      name: 'Ett par',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 2;
        },
        (value: number) => {
          return value <= 6 * 2;
        },
        (value: number) => {
          return value % 2 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else if (dice.length === 1)
          return [dice[0]];
        else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 2,
    });
    this.scores.set('twopair', {
      name: 'Tvåpar',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 6;
        },
        (value: number) => {
          return value <= 6 * 2 + 5 * 2;
        },
        (value: number) => {
          return value % 2 === 0;
        },
        (value: number) => {
          for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
              if (i !== j && value === i * 2 + j * 2) {
                return true;
              }

            }
          }
          return false;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = [1, 2, 3, 4, 5, 6];
        if (dice.length <= 1) {
          return valid;
        } else if (dice.length < 6) {
          return valid.filter(die => die !== dice[0]);
        } else {
          return [];
        }
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 4,
    });
    this.scores.set('triple', {
      name: 'Tretal',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 3;
        },
        (value: number) => {
          return value <= 6 * 3;
        },
        (value: number) => {
          return value % 3 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else if (dice.length === 1)
          return [dice[0], dice[0]];
        else if (dice.length === 2)
          return [dice[0]];
        else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 3,
    });
    this.scores.set('quadruple', {
      name: 'Fyrtal',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 4;
        },
        (value: number) => {
          return value <= 6 * 4;
        },
        (value: number) => {
          return value % 4 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 4,
    });
    this.scores.set('smallstraight', {
      name: 'Liten Stege',
      rules: [
        (value: number) => {
          return value === 15;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5];
        else
          return [];
      },
      diceFunction: (die: number) => {
        return [1, 2, 3, 4, 5];
      },
    });
    this.scores.set('bigstraight', {
      name: 'Stor Stege',
      rules: [
        (value: number) => {
          return value === 20;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [2, 3, 4, 5, 6];
        else
          return [];
      },
      diceFunction: (die: number) => {
        return [2, 3, 4, 5, 6];
      },
    });
    this.scores.set('fullhouse', {
      name: 'Kåk',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 7;
        },
        (value: number) => {
          return value <= 6 * 3 + 5 * 2;
        },
        (value: number) => {
          for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
              if (i !== j && value === i * 3 + j * 2) {
                return true;
              }

            }
          }
          return false;
        },
      ],
      validDice: (dice: number[]) => {
        let valid = [1, 2, 3, 4, 5, 6];
        if (dice.length <= 1) {
          return valid;
        }
        const die0 = dice.filter(die => die === dice[0]);
        const diceLeft = dice.filter(die => die !== die0[0]);
        const die1 = dice.filter(die => die === diceLeft[0]);

        if (die0.length > 0 && die1.length > 0) {
          valid = valid.filter(die => die === die0[0] || die === die1[0]);
        }
        if (die0.length >= 3) {
          valid = valid.filter(die => die !== die0[0]);
        }
        if (die1.length >= 3) {
          valid = valid.filter(die => die !== die1[0]);
        }
        if (dice.length < 5) {
          return valid;
        } else
          return [];
      },
      numberOfDice: 5,
    });
    this.scores.set('chance', {
      name: 'Chans',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 5;
        },
        (value: number) => {
          return value <= 6 * 5;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length < 5) {
          return [1, 2, 3, 4, 5, 6];
        } else {
          return [];
        }
      },
    });
    this.scores.set('yatzy', {
      name: 'Yatzy',
      rules: [
        (value: number) => {
          return value === 50;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [50];
        else
          return [];
      },
    });
    this.scores.set('totalsum', {
      name: 'Summa',
      value: 0,
      valueFuction: function (player: Player) {
        let sum = 0;
        for (const [key, score] of player.scores.entries()) {
          if (score.value &&
              key !== 'totalsum' &&
              key !== 'numbersum') {
            sum += score.value;
          }
        }
        return sum;
      },
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
      ],
      validDice: () => {
        return [];
      },
    });
  }
}

export class YatzyMaxi implements GameType {
  scores: Map<string, Score>;
  name: string = 'YatzyMaxi';

  constructor() {
    this.scores = new Map<string, Score>();
    this.scores.set('ones', {
      name: 'Ettor',
      valueOk: 75 * (1 / 21),
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value <= 6;
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value % 1 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(6).fill(1);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('twos', {
      name: 'Tvåor',
      valueOk: 75 * (2 / 21),
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 2 * 6;
        },
        (value: number) => {
          return value % 2 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(6).fill(2);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('threes', {
      name: 'Treor',
      valueOk: 75 * (3 / 21),
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 3 * 6;
        },
        (value: number) => {
          return value % 3 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(6).fill(3);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('fours', {
      name: 'Fyror',
      valueOk: 75 * (4 / 21),
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 4 * 6;
        },
        (value: number) => {
          return value % 4 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(6).fill(4);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('fives', {
      name: 'Femmor',
      valueOk: 75 * (5 / 21),
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 5 * 6;
        },
        (value: number) => {
          return value % 5 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(6).fill(5);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('sixes', {
      name: 'Sexor',
      valueOk: 75 * (6 / 21),
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 0;
        },
        (value: number) => {
          return value <= 6 * 6;
        },
        (value: number) => {
          return value % 6 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = Array<number>(6).fill(6);
        return dice.length === 0 ? valid : [];
      },
      diceFunction: (die: number, index: number) => {
        const dice = new Array<number>(index + 1);
        dice.fill(die);
        return dice;
      },
    });
    this.scores.set('numbersum', {
      name: 'Summa',
      value: 0,
      valueFuction: function (player: Player) {
        const sum: number = (player.scores.get('ones')?.value || 0) +
             (player.scores.get('twos')?.value || 0) +
             (player.scores.get('threes')?.value || 0) +
             (player.scores.get('fours')?.value || 0) +
             (player.scores.get('fives')?.value || 0) +
             (player.scores.get('sixes')?.value || 0);
        return sum;
      },
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
      ],
      validDice: () => {
        return [];
      },
    });
    this.scores.set('bonus', {
      name: 'Bonus',
      valueFuction: function (player: Player) {
        let sum = 0;
        sum = (player.scores.get('ones')?.value || 0) +
             (player.scores.get('twos')?.value || 0) +
             (player.scores.get('threes')?.value || 0) +
             (player.scores.get('fours')?.value || 0) +
             (player.scores.get('fives')?.value || 0) +
             (player.scores.get('sixes')?.value || 0);
        if (sum >= 75) {
          return 50;
        } else {
          return undefined;
        }
      },
      rules: [
        (value: number) => {
          return Number.isInteger(value) || value === null;
        },
      ],
      validDice: () => {
        return [];
      },
    });
    this.scores.set('pair', {
      name: 'Ett par',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 2;
        },
        (value: number) => {
          return value <= 6 * 2;
        },
        (value: number) => {
          return value % 2 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else if (dice.length === 1)
          return [dice[0]];
        else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 2,
    });
    this.scores.set('twopair', {
      name: 'Tvåpar',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 6;
        },
        (value: number) => {
          return value <= 6 * 2 + 5 * 2;
        },
        (value: number) => {
          return value % 2 === 0;
        },
        (value: number) => {
          for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
              if (i !== j && value === i * 2 + j * 2) {
                return true;
              }

            }
          }
          return false;
        },
      ],
      validDice: (dice: number[]) => {
        let valid = [1, 2, 3, 4, 5, 6];
        if (dice.length <= 1) {
          return valid;
        }
        const die0 = dice.filter(die => die === dice[0]);
        const diceLeft = dice.filter(die => die !== die0[0]);
        const die1 = dice.filter(die => die === diceLeft[0]);

        if (die0.length === 1 && die1.length === 1) {
          valid = valid.filter(die => die === die0[0] || die === die1[0]);
        } else if (die1.length === 1) {
          valid = valid.filter(die => die === die1[0]);
        }
        if (die0.length >= 2) {
          valid = valid.filter(die => die !== die0[0]);
        }
        if (die1.length >= 2) {
          valid = valid.filter(die => die !== die1[0]);
        }
        if (dice.length < 4) {
          return valid;
        } else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 4,
    });
    this.scores.set('threepair', {
      name: 'Trepar',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 2 + 4 + 6;
        },
        (value: number) => {
          return value <= 6 * 2 + 5 * 2 + 4 * 2;
        },
        (value: number) => {
          return value % 2 === 0;
        },
        (value: number) => {
          for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
              for (let k = 1; k <= 6; k++) {
                if (i !== j &&
                    i !== k &&
                    j !== k &&
                    value === i * 2 + j * 2 + k * 2) {
                  return true;
                }
              }
            }
          }
          return false;
        },
      ],
      validDice: (dice: number[]) => {
        let valid = [1, 2, 3, 4, 5, 6];
        const die0 = dice.filter(die => die === dice[0]);
        let diceLeft = dice.filter(die => die !== die0[0]);
        const die1 = dice.filter(die => die === diceLeft[0]);
        diceLeft = dice.filter(die => die !== die0[0] && die !== die1[0]);
        const die2 = dice.filter(die => die === diceLeft[0]);

        if ( die2.length >= 1) {
          valid = valid.filter(die => die === die0[0] || die === die1[0] && die === die2[0]);
        }
        if (die0.length >= 2) {
          valid = valid.filter(die => die !== die0[0]);
        }
        if (die1.length >= 2) {
          valid = valid.filter(die => die !== die1[0]);
        }
        if (die2.length >= 2) {
          valid = valid.filter(die => die !== die2[0]);
        }
        if (dice.length < 6) {
          return valid;
        } else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 6,
    });
    this.scores.set('triple', {
      name: 'Tretal',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 3;
        },
        (value: number) => {
          return value <= 6 * 3;
        },
        (value: number) => {
          return value % 3 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else if (dice.length === 1)
          return [dice[0], dice[0]];
        else if (dice.length === 2)
          return [dice[0]];
        else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 3,
    });
    this.scores.set('quadruple', {
      name: 'Fyrtal',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 4;
        },
        (value: number) => {
          return value <= 6 * 4;
        },
        (value: number) => {
          return value % 4 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else if (dice.length === 1)
          return [dice[0], dice[0], dice[0]];
        else if (dice.length === 2)
          return [dice[0], dice[0]];
        else if (dice.length === 3)
          return [dice[0]];
        else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 4,
    });
    this.scores.set('quintuple', {
      name: 'Femtal',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 5;
        },
        (value: number) => {
          return value <= 6 * 5;
        },
        (value: number) => {
          return value % 5 === 0;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else
          return [];
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        dice.push(die);
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 5,
    });
    this.scores.set('smallstraight', {
      name: 'Liten Stege',
      rules: [
        (value: number) => {
          return value === 15;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5];
        else
          return [];
      },
      diceFunction: (die: number) => {
        return [1, 2, 3, 4, 5];
      },
    });
    this.scores.set('bigstraight', {
      name: 'Stor Stege',
      rules: [
        (value: number) => {
          return value === 20;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [2, 3, 4, 5, 6];
        else
          return [];
      },
      diceFunction: (die: number) => {
        return [2, 3, 4, 5, 6];
      },
    });
    this.scores.set('fullstraight', {
      name: 'Full stege',
      rules: [
        (value: number) => {
          return value === 21;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [1, 2, 3, 4, 5, 6];
        else
          return [];
      },
      diceFunction: (die: number) => {
        return [1, 2, 3, 4, 5, 6];
      },
    });
    this.scores.set('fullhouse', {
      name: 'Kåk',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 7;
        },
        (value: number) => {
          return value <= 6 * 3 + 5 * 2;
        },
        (value: number) => {
          for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
              if (i !== j && value === i * 3 + j * 2) {
                return true;
              }

            }
          }
          return false;
        },
      ],
      validDice: (dice: number[]) => {
        let valid = [1, 2, 3, 4, 5, 6];
        if (dice.length <= 1) {
          return valid;
        }
        const die0 = dice.filter(die => die === dice[0]);
        const diceLeft = dice.filter(die => die !== die0[0]);
        const die1 = dice.filter(die => die === diceLeft[0]);

        if (die0.length > 0 && die1.length > 0) {
          valid = valid.filter(die => die === die0[0] || die === die1[0]);
        }
        if (die0.length >= 3) {
          valid = valid.filter(die => die !== die0[0]);
        }
        if (die1.length >= 3) {
          valid = valid.filter(die => die !== die1[0]);
        }
        if (dice.length < 5) {
          return valid;
        } else
          return [];
      },
      numberOfDice: 5,
    });
    this.scores.set('villa', {
      name: 'Dubbelt tretal',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 3 + 6;
        },
        (value: number) => {
          return value <= 6 * 3 + 5 * 3;
        },
        (value: number) => {
          for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
              if (i !== j && value === i * 3 + j * 3) {
                return true;
              }

            }
          }
          return false;
        },
      ],
      validDice: (dice: number[]) => {
        const valid = [1, 2, 3, 4, 5, 6];
        if (dice.length <= 1) {
          return valid;
        } else if (dice.length < 6) {
          return valid.filter(die => die !== dice[0]);
        } else {
          return [];
        }
      },
      diceFunction: (die: number) => {
        const dice = new Array<number>();
        dice.push(die);
        dice.push(die);
        dice.push(die);
        return dice;
      },
      numberOfDice: 6,
    });
    this.scores.set('tower', {
      name: 'Torn',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 1 * 4 + 2 * 2;
        },
        (value: number) => {
          return value <= 6 * 4 + 5 * 2;
        },
        (value: number) => {
          for (let i = 1; i <= 6; i++) {
            for (let j = 1; j <= 6; j++) {
              if (i !== j && value === i * 4 + j * 2) {
                return true;
              }
            }
          }
          return false;
        },
      ],
      validDice: (dice: number[]) => {
        let valid = [1, 2, 3, 4, 5, 6];
        if (dice.length <= 1) {
          return valid;
        }
        const die0 = dice.filter(die => die === dice[0]);
        const diceLeft = dice.filter(die => die !== die0[0]);
        const die1 = dice.filter(die => die === diceLeft[0]);

        if (die0.length > 0 && die1.length > 0) {
          valid = valid.filter(die => die === die0[0] || die === die1[0]);
        }
        if (die0.length === 3 && die1.length === 2) {
          valid = valid.filter(die => die === die0[0]);
        }
        if (die0.length === 2 && die1.length === 3) {
          valid = valid.filter(die => die === die1[0]);
        }
        if (die0.length >= 4) {
          valid = valid.filter(die => die !== die0[0]);
        }
        if (die1.length >= 4) {
          valid = valid.filter(die => die !== die1[0]);
        }
        if (dice.length < 6) {
          return valid;
        } else
          return [];
      },
      numberOfDice: 6,
    });
    this.scores.set('chance', {
      name: 'Chans',
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
        (value: number) => {
          return value >= 6;
        },
        (value: number) => {
          return value <= 6 * 6;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length < 6) {
          return [1, 2, 3, 4, 5, 6];
        } else {
          return [];
        }
      },
      numberOfDice: 6,
    });
    this.scores.set('maxiyatzy', {
      name: 'MaxiYatzy',
      rules: [
        (value: number) => {
          return value === 100;
        },
      ],
      validDice: (dice: number[]) => {
        if (dice.length === 0)
          return [100];
        else
          return [];
      },
      diceFunction: (die: number) => {
        return 100;
      },
    });
    this.scores.set('totalsum', {
      name: 'Summa',
      value: 0,
      valueFuction: function (player: Player) {
        let sum = 0;
        for (const [key, score] of player.scores.entries()) {
          if (score.value &&
              key !== 'totalsum' &&
              key !== 'numbersum') {
            sum += score.value;
          }
        }
        return sum;
      },
      rules: [
        (value: number) => {
          return Number.isInteger(value);
        },
      ],
      validDice: () => {
        return [];
      },
    });
  }
}
