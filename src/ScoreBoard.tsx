import React from 'react';

import './App.css';
import { GameType } from './GameTypes';
import Player from './Player';


interface ScoreBoardProps {
  gameType: GameType;
  players: Player[];
  onChange: (player: Player, scoreId: string, value: number) => void;
  onBlur: (player: Player, scoreId: string, value: number) => boolean;
  onClick: (player: Player, scoreId: string) => void;
  useKeyboard: boolean;
}

export class ScoreBoard extends React.Component<ScoreBoardProps> {
  testForMap() {
    const rows = [];
    for (const [scoreKey, score] of this.props.gameType.scores) {
      rows.push(<ScoreRow
        title={score.name}
        id={scoreKey}
        className={'score-row'}
        players={this.props.players}
        onChange={this.props.onChange}
        onBlur={this.props.onBlur}
        onClick={this.props.onClick}
        useKeyboard={this.props.useKeyboard}
        key={scoreKey + '-row'}
      />);
    }
    return rows;
  }

  render() {
    return (
      <table className="score-board">
        <tbody>
          <ScoreHeader players={this.props.players} key="header" />
          <>{this.testForMap()}</>
        </tbody>
      </table>
    );
  }
}

interface ScoreRowProps {
  title: string;
  players: Player[];
  id: string;
  className: string;
  onChange: (player: Player, scoreId: string, value: number) => void;
  onBlur: (player: Player, scoreId: string, value: number) => boolean;
  onClick: (player: Player, scoreId: string) => void;
  useKeyboard: boolean;
}

class ScoreRow extends React.Component<ScoreRowProps> {
  render() {
    try {
    return (
      <tr className={this.props.className} id={this.props.id + '-row'}>
        <th>{this.props.title}</th>
        {this.props.players.map((player) => {
          return <ScoreCell
            value={player.scores.get(this.props.id)?.value}
            inputDisabled={player.scores.get(this.props.id)?.valueFuction !== undefined}
            onChange={this.props.onChange}
            onBlur={this.props.onBlur}
            onClick={this.props.onClick}
            useKeyboard={this.props.useKeyboard}
            player={player}
            id={this.props.id}
            key={this.props.id + '-' + player.id}
          />
        })}
      </tr>);
    } catch (e) {
        alert(e);
    }
  }
}

interface ScoreCellProps {
  value: number | undefined;
  player: Player;
  id: string;
  inputDisabled: boolean;
  onChange: (player: Player, scoreId: string, value: number) => void;
  onBlur: (player: Player, scoreId: string, value: number) => boolean;
  onClick: (player: Player, scoreId: string) => void;
  useKeyboard: boolean;
}
class ScoreCell extends React.Component<ScoreCellProps> {
  handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    const value = event.target.value === '-' ? 0 : event.target.value.replaceAll('/[^\\d]', '');
    if (!isNaN(Number(value))) {
      this.props.onChange(this.props.player, this.props.id, Number(value));
    }
  }
  cellRef = React.createRef<HTMLTableCellElement>();

  componentDidUpdate(prevProps: ScoreCellProps) {
    if (this.props.value !== prevProps.value) {
      const value = this.props.value;
      const vOk = this.props.player.scores.get(this.props.id)?.valueOk;
      const element = this.cellRef.current;
      if (value !== null && vOk !== undefined && vOk !== null && element !== undefined && element !== null) {
        this.changeScoreColor(value, vOk, element)
      }
    }
  }

  handleFocus(event: React.FocusEvent<HTMLInputElement>) {
    if (!this.props.useKeyboard) {
      event.target.blur();
    } else {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      event.target.parentElement!.className = event.target.parentElement!.className.replaceAll(' wrong-value', '');
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      event.target.parentElement!.className += ' cell-selected';
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      event.target.parentElement!.parentElement!.className += ' row-selected';
    }
  }

  handleBlur(event: React.FocusEvent<HTMLInputElement>) {
    const value = event.target.value.replaceAll('/[^\\d]', '');
    let valueNumber: number;
    if (value !== '' && !isNaN(Number(value))) {
      valueNumber = Number(value);
      const valid = this.props.onBlur(this.props.player, this.props.id, valueNumber);
      if (!valid) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        event.target.parentElement!.className += ' wrong-value';
      }
    }

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    event.target.parentElement!.className =
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      event.target.parentElement!.className.replace(' cell-selected', '');
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    event.target.parentElement!.parentElement!.className =
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      event.target.parentElement!.parentElement!.className.replace(' row-selected', '');
  }

  handleClick() {
    this.props.onClick(this.props.player, this.props.id);
  }
  changeScoreColor(value: number | undefined, valueOk: number, element: HTMLElement) {
    if(value !== undefined) {
      element.className =
        element.className.replaceAll(' ok-value', '').replaceAll(' good-value', '').replaceAll(' low-value', '');
      if (value === valueOk) {
        element.className += ' ok-value';
      } else if (value > valueOk) {
        element.className += ' good-value';
      } else if (value < valueOk && value > 0) {
        element.className += ' low-value';
      }
    }
  }
  setInput() {
    let inputValue = this.props.value !== undefined ? this.props.value : '';
    const score = this.props.player.scores.get(this.props.id);
    if (score !== undefined && score.valueFuction === undefined) {
      inputValue = inputValue === 0 ? '-' : inputValue;
    }
    if (this.props.useKeyboard) {
      return <input type='text' inputMode='numeric'
        value={inputValue}
        disabled={this.props.inputDisabled}
        onFocus={(e) => this.handleFocus(e)}
        onBlur={(e) => this.handleBlur(e)}
        onChange={(e) => this.handleChange(e)}
      />
    } else {
      return <input type='text' inputMode='numeric'
        value={inputValue}
        disabled={this.props.inputDisabled}
        onFocus={(e) => this.handleFocus(e)}
        onClick={() => this.handleClick()}
        onBlur={(e) => this.handleBlur(e)}
        onChange={(e) => this.handleChange(e)}
      />
    }
  }

  render() {
    return (
      <td ref={this.cellRef} id={'cell-' + this.props.player.id + '-' + this.props.id}>
        {this.setInput()}
      </td>
    );
  }
}

interface ScoreHeaderProps {
  players: Player[];
}

class ScoreHeader extends React.Component<ScoreHeaderProps> {
  render() {
    return (
      <tr className="score-header">
        <th></th>
        {this.props.players.map((player) => (
          <th key={"head-" + player.id}>{player.name}</th>
        ))}
      </tr>
    );
  }
}
