import React from 'react';
import { cloneDeep } from 'lodash';

import Player from './Player';
import { Modal, Button } from './BaseComponents';
import { GameType, YatzyStandard, YatzyMaxi} from './GameTypes';
import { ScoreBoard } from './ScoreBoard';

interface GameState {
  gameType: GameType;
  players: Player[];
  playerName: string;
  dice: number[];
  dicePlayer: Player | null;
  diceScore: string;
  displayPlayerModal: boolean;
  displayDiceModal: boolean;
  warningModal: boolean;
  changeToType: string | null;
  useKeyboard: boolean;
  darkMode: boolean;
}
interface GameProps {
  gameType: GameType;
}
class Game extends React.Component<GameProps, GameState> {
  constructor(props: GameProps) {
    super(props);
    this.state = {
      gameType: props.gameType,
      players: [],
      playerName: '',
      dice: [],
      dicePlayer: null,
      diceScore: '',
      displayPlayerModal: false,
      displayDiceModal: false,
      warningModal: false,
      changeToType: null,
      useKeyboard: false,
      darkMode: true,
    }
  }

  onChange = (player: Player, id: string, value: number) => {
    this.setState((state) => {
      const players = cloneDeep(state.players);
      const playerIndex = players.findIndex(pl => pl.id === player.id);
      if (players[playerIndex].scores.get(id)?.valueFuction === undefined) {
        players[playerIndex].scores.get(id)!.value = value;
      }
      return { players: players };
    });
  };

  updateCell = (player: Player, id: string, value: number) => {
    let valid = true;
    player = cloneDeep(player);
    const players = cloneDeep(this.state.players);
    const score = player.scores.get(id);

    if (score !== undefined) {
      score.rules.forEach(rule => {
        if (score.value && rule(value) === false) {
          valid = false;
        }
      });
      if (valid && score.valueFuction === undefined && value !== undefined) {
        score.value = value;
      }
      else {
        score.value = undefined;
      }
      player.scores.forEach((score) => {
        if (score.valueFuction !== undefined) {
          score.value = score.valueFuction(player);
        }
      });
    }

    const playerIndex = players.findIndex(pl => pl.id === player.id);
    players[playerIndex] = player;
    this.setState({ players: players });

    return valid;
  };

  addPlayer(playerName: string) {
    this.setState((state) => {
      const players = cloneDeep(state.players);
      let id = playerName.trim().toLowerCase();
      players.forEach((player) => {
        let i = 1;
        if (player.id === id) {
          id += ++i;
        }
      });
      players.push(new Player(playerName, id, this.state.gameType));
      return { players: players, playerName: '' };
    });
    this.dismissPlayerModal();
  }
  dismissPlayerModal() {
    this.setState({ displayPlayerModal: false });
  }
  dismissDiceModal() {
    this.setState({ displayDiceModal: false });
  }

  addDie(die: number, index: number) {
    const score = this.state.gameType.scores.get(this.state.diceScore);
    let dice = cloneDeep(this.state.dice);
    if(score !== undefined && score.diceFunction !== undefined) {
      dice = dice.concat(score.diceFunction!(die, index));
    } else {
      dice.push(die);
    }
    this.setState({ dice: dice });
  }

  updateAvailableDice() {
    const validDice = this.state.gameType.scores.get(this.state.diceScore)?.validDice !== undefined ?
      this.state.gameType.scores.get(this.state.diceScore)!.validDice(this.state.dice) : [];
    const dice = validDice.map((die, index) => (
      <button onClick={() => this.addDie(die, index)}
        className={'die'}
        key={this.state.diceScore + '-' + index + '-button'}
      >
        {(() => {
          switch (die) {
            case 1:
              return '⚀';
            case 2:
              return '⚁';
            case 3:
              return '⚂';
            case 4:
              return '⚃';
            case 5:
              return '⚄';
            case 6:
              return '⚅';
            case 50:
              return '5×⚄';
            case 100:
              return '6×⚄';
            default:
              return die;
          }
        })()}
      </button>
    ));
    return dice;
  }

  displayAddPlayerModal() {
    this.setState(() => {
      return { displayPlayerModal: true }
    });
  }

  displayDicePickerModal = (player: Player, id: string) => {
    this.setState(() => {
      return { displayDiceModal: true, dicePlayer: player, diceScore: id, dice: [] }
    });
  }
  addDice = (player: Player) => {
    const numberOfDice = this.state.gameType.scores.get(this.state.diceScore)?.numberOfDice;

    if (numberOfDice === undefined && this.state.dice.length > 0) {
      this.updateCell(player, this.state.diceScore, this.state.dice.reduce((a, b) => a + b));
      this.dismissDiceModal();
    } else if (this.state.dice.length === numberOfDice) {
      this.updateCell(player, this.state.diceScore, this.state.dice.reduce((a, b) => a + b));
      this.dismissDiceModal();
    }
  }
  changeGameType = (type: string) => {
    this.setState({changeToType: type}, () => {
      if (this.state.players.length > 0) {
        this.displayWarningModal();
      } else {
        this.actuallyChangeGameType();
      }
    });
  }
  actuallyChangeGameType() {
    if (this.state.changeToType === 'YatzyStandard') {
      this.setState(() => {return {gameType: new YatzyStandard(), players: [], changeToType: null}});
    } else if (this.state.changeToType === 'YatzyMaxi') {
      this.setState(() => {return {gameType: new YatzyMaxi(), players: [], changeToType: null}});
    }
    this.dismissWarningModal();
  }
  displayWarningModal = () => {
    this.setState(() => {
      return { warningModal: true }
    });
  }
  dismissWarningModal = () => {
    this.setState(() => {
      return { warningModal: false }
    });
  }

  render() {
    return (
      <div className={'game' + (this.state.darkMode ? ' dark' : ' light')}>
        <header className={'app-header'}>
          <button
            onClick={() => this.displayAddPlayerModal()}
            className={'neutral'}
            key={'add-player-button'}
          >
            + Spelare
          </button>
          <button
            onClick={() => this.changeGameType('YatzyStandard')}
            className={'neutral left-button' + (this.state.gameType.name == 'YatzyStandard' ? ' activated' : '')}
            key={'yatzy-standard-button'}
          >
            ⚄
          </button>
          <button
            onClick={() => this.changeGameType('YatzyMaxi')}
            className={'neutral right-button' + (this.state.gameType.name == 'YatzyMaxi' ? ' activated' : '')}
            key={'yatzy-maxi-button'}
          >
            ⚅
          </button>
          <button
            onClick={() => this.setState({ useKeyboard: false })}
            className={'neutral left-button' + (!this.state.useKeyboard ? ' activated' : '')}
            key={'touch-button'}
          >
            👆
          </button>
          <button
            onClick={() => this.setState({ useKeyboard: true })}
            className={'neutral right-button' + (this.state.useKeyboard ? ' activated' : '')}
            key={'keyboard-button'}
          >
            ⌨️
          </button>
          <button
            onClick={() => this.setState({ darkMode: true })}
            className={'neutral left-button' + (this.state.darkMode ? ' activated' : '')}
            key={'dark-mode-button'}
          >
            🌛
          </button>
          <button
            onClick={() => this.setState({ darkMode: false })}
            className={'neutral right-button' + (!this.state.darkMode ? ' activated' : '')}
            key={'light-mode-button'}
          >
            🌞
          </button>
        </header>
        <Modal
          id='player-modal'
          header='Lägg till spelare'
          display={this.state.displayPlayerModal}
          content={<React.Fragment>
            <input type='text'
              value={this.state.playerName}
              onChange={(e) => this.setState({ playerName: e.target.value })}
            />
          </React.Fragment>}
          buttons={<React.Fragment>
            <Button id='add' value='Lägg till'
              color='green'
              action={() => this.addPlayer(this.state.playerName)}
            />
            <Button id='cancel' value='Avbryt'
              color='red'
              action={() => this.dismissPlayerModal()}
            />
          </React.Fragment>}
        />
        <Modal
          id='dice-modal'
          header={'Välj ' + this.state.gameType.scores.get(this.state.diceScore)?.name}
          display={this.state.displayDiceModal}
          content={<React.Fragment>
            <div className='chosen'>
              {this.state.dice.map((die) => {
                switch (die) {
                  case 1:
                    return '⚀';
                  case 2:
                    return '⚁';
                  case 3:
                    return '⚂';
                  case 4:
                    return '⚃';
                  case 5:
                    return '⚄';
                  case 6:
                    return '⚅';
                  case 50:
                    return '5×⚄';
                  case 100:
                    return '6×⚄';
                  default:
                    return die;
                }
              })}
            </div>
            <div className='dice-buttons'>
              {this.updateAvailableDice()}
            </div>
          </React.Fragment>}
          buttons={<React.Fragment>
            <Button id='choose' value='Välj'
              color='green'
              action={() => { this.addDice(this.state.dicePlayer!) }}
            />
            <Button id='zero' value='Stryk'
              action={() => { this.updateCell(this.state.dicePlayer!, this.state.diceScore, 0); this.dismissDiceModal(); }}
            />
            <Button id='cancel' value='Avbryt'
              color='red'
              action={() => this.dismissDiceModal()}
            />
          </React.Fragment>}
        />
        <Modal
          id='warning-modal'
          header='Varning'
          display={this.state.warningModal}
          content={<React.Fragment>
            Om du ändrar spelläge nollställs poängen. fortsätt?
          </React.Fragment>}
          buttons={<React.Fragment>
            <Button id='change' value='Byt spelläge'
              color='red'
              action={() => this.actuallyChangeGameType()}
            />
            <Button id='cancel' value='Avbryt'
              action={() => this.dismissWarningModal()}
            />
          </React.Fragment>}
        />
        <ScoreBoard gameType={this.state.gameType} players={this.state.players}
          onChange={this.onChange}
          onBlur={this.updateCell}
          onClick={this.displayDicePickerModal}
          useKeyboard={this.state.useKeyboard}
        />
      </div>
    )
  }
}
export default Game;
